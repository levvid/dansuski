<?php
//define ("DEBUG_MODE",1);
//define ("DEBUG_SQL",1);
define ("LOG_DIR",str_replace('/php','',getcwd()."/logs/"));
function WriteToLog($msg,$log_file = "log.txt") {
    $name = LOG_DIR.$log_file;
    if (@strlen(file_get_contents($name)) > 100*1024*1024)
       @unlink($name);
		if (!file_exists($name)) {
			file_put_contents($name,"");
			chmod($name,0777);
		}
    $file = @fopen($name, "a");
//    $ip = @$_SERVER["REMOTE_ADDR"];
    @fwrite($file, @date("d.m.Y H:i:s")."\t".$msg."\n");
    @fclose($file);
}


require_once($_SERVER['DOCUMENT_ROOT'].'/php/db_config.php');

$db_airp = new PDO("mysql:host=".$server.";dbname=".$database_airports, $username, $password);
$db_routes = new PDO("mysql:host=".$server.";dbname=".$database_routes, $username, $password);
$db_equipment = new PDO("mysql:host=".$server.";dbname=".$database_equipment, $username, $password);
	
// executing query for Update
function ExecuteQueryWitoutResult($query, $params, $db)
{
	try {
		$res = $db->prepare($query);
		$res->execute($params);
if (defined("DEBUG_SQL")) WriteToLog("Execute query: ".$query."\n".print_r($params,true)."\nDATABASE: ".$db->query('select database()')->fetchColumn()."\nRESULT:\n".print_r($res,true),"sql.txt");
	return $res;
	} catch (PDOException $e) {
    $msg = "DB query error: " . $e->getMessage()."\n".$query."\n".print_r($params,true)."\nDATABASE: ".$db->query('select database()')->fetchColumn();
if (defined("DEBUG_SQL")) WriteToLog($msg,"sql.txt");
		return null;
  } // catch
}

// executes query
function ExecuteQuery($query, $params, $db)
{
	try {
		$res = $db->prepare($query);
		$res->execute($params);
		$result = $res->fetchAll(PDO::FETCH_ASSOC);
if (defined("DEBUG_SQL")) WriteToLog("Open query: ".$query."\n".print_r($params,true)."\nDATABASE: ".$db->query('select database()')->fetchColumn()."\nRESULT:\n".print_r($result,true),"sql.txt");
		return $result;
	} catch (PDOException $e) {
    $msg = "DB query error: " . $e->getMessage()."\n".$query."\n".print_r($params,true)."\nDATABASE: ".$db->query('select database()')->fetchColumn();
if (defined("DEBUG_SQL")) WriteToLog($msg,"sql.txt");
		return null;
  } // catch
}

/*
http://www.whatflieswhere.com/php/map_functions.php?op=allData&filter%5Baircraft_type1%5D=choose&filter%5Baircraft_type2%5D=choose
http://www.whatflieswhere.com/php/map_functions.php?op=allData&filter%5Baircraft_type1%5D=320&filter%5Baircraft_type2%5D=choose
http://www.whatflieswhere.com/php/map_functions.php?op=allData&filter%5Baircraft_type1%5D=320&filter%5Baircraft_type2%5D=737
 */
function getData($db,$filter)
{
if (defined("DEBUG_MODE")) WriteToLog(">getData filter:\n".print_r($filter,true));
	$ar_airports_filter = array();
	$ar_airlines_filter = array();
	if (strtolower($filter["aircraft_type1"]) != "choose") {
		$sql = "SELECT destinations, airlines FROM equipment WHERE iata = ?";
		$qr = ExecuteQuery($sql,array($filter["aircraft_type1"]),$db);
		if (count($qr) > 0) {
			foreach ($qr as $row) {
				$ar_dest = explode("|",$row["destinations"]);
				foreach ($ar_dest as $iata) {
					$iata = trim($iata);
					if ((strlen($iata) >= 3) && (!array_key_exists($iata,$ar_airports_filter)))
						$ar_airports_filter[$iata] = 1;
				}
				$ar_lines = explode("|",$row["airlines"]);
				foreach ($ar_lines as $line) {
					$line = trim($line);
					if ((strlen($line) >= 2) && (!array_key_exists($line,$ar_airlines_filter)))
						$ar_airlines_filter[$line] = 1;
				}
			}
		}
	} // filter aircraft type 1
	if (strtolower($filter["aircraft_type2"]) != "choose") {
		$sql = "SELECT destinations, airlines FROM equipment WHERE iata = ?";
		$qr = ExecuteQuery($sql,array($filter["aircraft_type2"]),$db);
		if (count($qr) > 0) {
			foreach ($qr as $row) {
				$ar_dest = explode("|",$row["destinations"]);
				foreach ($ar_dest as $iata) {
					$iata = trim($iata);
					if ((strlen($iata) >= 3) && (!array_key_exists($iata,$ar_airports_filter)))
						$ar_airports_filter[$iata] = 1;
				}
				$ar_lines = explode("|",$row["airlines"]);
				foreach ($ar_lines as $line) {
					$line = trim($line);
					if ((strlen($line) >= 2) && (!array_key_exists($line,$ar_airlines_filter)))
						$ar_airlines_filter[$line] = 1;
				}
			}
		}
	} // filter aircraft type 2
	$sql = "SELECT iata, city, lat, lon, rank_tier 
					FROM geo_stations 
					WHERE (`type`='A') AND (rank_tier <= 7)";
	if (count($ar_airports_filter) > 0) {
		$sql .= " AND (`iata` IN ('".implode("','",array_keys($ar_airports_filter))."'))";
	}
	$airports = ExecuteQuery($sql, null, $db);
if (defined("DEBUG_MODE")) { WriteToLog("airports count = ".count($airports)); file_put_contents(LOG_DIR."airports.txt",implode("\n",$airports)); }
	
	$sql = "SELECT iata, airline, alliance FROM airlines WHERE active = 1 GROUP BY iata";
	if (count($ar_airlines_filter) > 0) {
		$sql .= " AND (`iata` IN ('".implode("','",array_keys($ar_airlines_filter))."'))";
	}
	$airlines = ExecuteQuery($sql, null, $db);
if (defined("DEBUG_MODE")) { WriteToLog("airlines count = ".count($airlines)); file_put_contents(LOG_DIR."airlines.txt",implode("\n",$airlines)); }

	$res = json_encode(array(
		"state" => "ok",
		"airports" => $airports,
		"airlines" => $airlines
	));
  return $res;
}

function getDays($route)
{
	$days = $route["day1"].$route["day2"].$route["day3"].$route["day4"].$route["day5"].$route["day6"].$route["day7"];
	$days = str_replace("Y","1", str_replace("N","0", $days));
	return bindec($days);
}

function getRoutes($db, $db_airp, $airport, $filter)
{
if (defined("DEBUG_MODE")) WriteToLog(">getRoutes airport = $airport filter:\n".print_r($filter,true));
	global $database_routes;
	$sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$database_routes."' AND TABLE_NAME=:airport";
	$params = array(":airport"=>$airport);
	$orig_aip = ExecuteQuery($sql, $params, $db);
// rewritten by Ilia Ivanov
	$ar_airports_filter = array();
	$ar_airlines_filter = array();
	if (strtolower($filter["aircraft_type1"]) != "choose") {
		$sql = "SELECT destinations, airlines FROM equipment WHERE iata = ?";
		$qr = ExecuteQuery($sql,array($filter["aircraft_type1"]),$db_airp);
		if (count($qr) > 0) {
			foreach ($qr as $row) {
				$ar_dest = explode("|",$row["destinations"]);
				foreach ($ar_dest as $iata) {
					$iata = trim($iata);
					if ((strlen($iata) >= 3) && (!array_key_exists($iata,$ar_airports_filter)))
						$ar_airports_filter[$iata] = 1;
				}
				$ar_lines = explode("|",$row["airlines"]);
				foreach ($ar_lines as $line) {
					$line = trim($line);
					if ((strlen($line) >= 2) && (!array_key_exists($line,$ar_airlines_filter)))
						$ar_airlines_filter[$line] = 1;
				}
			}
		}
	} // filter aircraft type 1
	if (strtolower($filter["aircraft_type2"]) != "choose") {
		$sql = "SELECT destinations, airlines FROM equipment WHERE iata = ?";
		$qr = ExecuteQuery($sql,array($filter["aircraft_type2"]),$db_airp);
		if (count($qr) > 0) {
			foreach ($qr as $row) {
				$ar_dest = explode("|",$row["destinations"]);
				foreach ($ar_dest as $iata) {
					$iata = trim($iata);
					if ((strlen($iata) >= 3) && (!array_key_exists($iata,$ar_airports_filter)))
						$ar_airports_filter[$iata] = 1;
				}
				$ar_lines = explode("|",$row["airlines"]);
				foreach ($ar_lines as $line) {
					$line = trim($line);
					if ((strlen($line) >= 2) && (!array_key_exists($line,$ar_airlines_filter)))
						$ar_airlines_filter[$line] = 1;
				}
			}
		}
	} // filter aircraft type 2
	$carriers_condition_0 = (count($ar_airlines_filter) > 0) ?" AND (`carrier` IN ('".implode("','",array_keys($ar_airlines_filter))."'))" : "";
	// filter carriers / aliases
	$carriers_mode = (int)$filter["carriers"]["mode"];
	$carriers_condition_1 = "";
	if (count($filter["carriers"]["ids"]) > 0) {
		if ($carriers_mode == 1) {
			// carriers
			$carriers_condition_1 =  " AND (`carrier` IN ('".implode("','",$filter["carriers"]["ids"])."'))";
		} else {
			// alliances
			$ar_filter_carriers = array();
			$qr = ExecuteQuery("SELECT airlines FROM airlines_alliances WHERE id IN (".implode(",",$filter["carriers"]["ids"]).")",null,$db_airp);
			if (count($qr) > 0)
				foreach ($qr as $row) {
					$ar = explode("|",$row["airlines"]);
					foreach ($ar as $airline) {
						$airline = trim($airline);
						if (strlen($airline) == 2) {
							$ar_filter_carriers[$airline] = 1;
						}
					}
				}
			$carriers_condition_1 = " AND (`carrier` IN ('".implode("','",array_keys($ar_filter_carriers))."'))";
		}
	}
	$res = array("state" => "ok", "origin" => $airport, "routes" => array());
	if (count($orig_aip)>0) {
		$routesTable = $airport;
		// aircrafts
		$aircrafts_equipment_condition = "";
		if ((strtolower($filter["aircraft_type1"]) != "choose")  && (strtolower($filter["aircraft_type2"]) != "choose"))
			$aircrafts_equipment_condition = " AND ((equipmentCode = '".$filter["aircraft_type1"]."') OR (equipmentCode = '".$filter["aircraft_type2"]."'))";
		if ((strtolower($filter["aircraft_type1"]) != "choose")  && (strtolower($filter["aircraft_type2"]) == "choose"))
			$aircrafts_equipment_condition = " AND (equipmentCode = '".$filter["aircraft_type1"]."')";
		if ((strtolower($filter["aircraft_type1"]) == "choose")  && (strtolower($filter["aircraft_type2"]) != "choose"))
			$aircrafts_equipment_condition = " AND (equipmentCode = '".$filter["aircraft_type2"]."')";
		// aircraft types
		$aircraft_type_condition = "";
		$equipment_condition = "(1)";
		if (((int)$filter["widebody"] > 0) && ((int)$filter["narrowbody"] <= 0)) {
				$equipment_condition .= " AND (`widebody` = 'Y')";
		}
		if (((int)$filter["widebody"] <= 0) && ((int)$filter["narrowbody"] > 0)) {
				$equipment_condition .= " AND (`widebody` = 'N')";
		}
		if (((int)$filter["widebody"] > 0) && ((int)$filter["narrowbody"] > 0)) {
				$equipment_condition .= " AND ((`widebody` = 'Y') OR (`widebody` = 'N'))";
		}
		if (((int)$filter["turboengine"] > 0) && ((int)$filter["jetengine"] <= 0)) {
				$equipment_condition .= " AND (`type` = 'Turboprop-engined aircraft')";
		}
		if (((int)$filter["turboengine"] <= 0) && ((int)$filter["jetengine"] > 0)) {
				$equipment_condition .= " AND (`type` = 'Jet-engined aircraft')";
		}
		if (((int)$filter["turboengine"] > 0) && ((int)$filter["jetengine"] > 0)) {
				$equipment_condition .= " AND ((`type` = 'Turboprop-engined aircraft') OR (`type` = 'Jet-engined aircraft'))";
		}
		$qr = ExecuteQuery("SELECT DISTINCT iata FROM equipment WHERE ".$equipment_condition,null,$db_airp);
		if (count($qr) > 0) {
			$aircraft_type_condition = " AND (equipmentCode IN (";
			foreach ($qr as $idx => $row) {
				if ($idx > 0) $aircraft_type_condition .= ",";
				$aircraft_type_condition .= "'".$row["iata"]."'";
			}
			$aircraft_type_condition .= "))";
		}
		// programs condition
		$programs_condition = "";
		if (count($filter["programs"]) > 0) {
			$qr = ExecuteQuery("SELECT airlines FROM credit_cards_programs WHERE id IN (".implode(",",$filter["programs"]).")",null,$db_airp);
			$ar_programs_carriers = array();
			if (count($qr) > 0) {
				foreach ($qr as $row) {
					$ar_lines = json_decode($row["airlines"],true);
					$ar_lines = @$ar_lines["airline_partners"];
					if ((is_array($ar_lines)) && (count($ar_lines) > 0)){
						foreach ($ar_lines as $ar_line) {
							$carrier = $ar_line["airline_iata"];
							$ar_programs_carriers[$carrier] = 1;
						}
					}
				}
			}
			if (count($ar_programs_carriers) > 0) {
				$programs_condition = " AND (`carrier` IN ('".implode("','",array_keys($ar_programs_carriers))."'))";
			}
		}
		// airport condition
		$airport_condition = (count($ar_airports_filter) > 0) ? " AND (`arrivalAirport` IN ('".implode("','",array_keys($ar_airports_filter))."'))" : "";
		$sql = "SELECT arrivalAirport,  departureTime, flightNumber, days, carrier 
						FROM " . $routesTable . " 
						WHERE (stops = 0) AND (departureAirport = ?) AND (isCodeshare = 0) ".$aircrafts_equipment_condition.$airport_condition.$carriers_condition_0.$carriers_condition_1.$aircraft_type_condition.$programs_condition."  
						GROUP BY flightNumber, arrivalAirport 
						ORDER BY effectiveDate DESC, flightNumber ASC";
		$data = ExecuteQuery( $sql, array($airport), $db );
if (defined("DEBUG_MODE")) WriteToLog("1.getRoutes airport = $airport sql:\n".$sql."\n".print_r($data,true));
//var_dump($data); die ($sql);
		foreach ( $data as $row ) {
			$res["routes"][] = array($row["arrivalAirport"], $row["arrivalAirport"], $row["departureTime"], $row["flightNumber"], $row["days"], $row["carrier"]);
		}
	}
	return json_encode($res);
/*
 	$res = array();
	if(count($orig_aip)>0)
	{
		$routesTable = $airport;
		
		
		$sql = "SELECT arrivalAirport, departureTime, flightNumber, days, carrier FROM ".$routesTable." where stops = '0' group by flightNumber, arrivalAirport ORDER BY effectiveDate DESC, flightNumber ASC";
		$data = ExecuteQuery($sql, null, $db);
		foreach($data as $row)
		{
			$days = $row['days'];
			$route = '["'.$row["arrivalAirport"].'", "'.$row["departureTime"].'", "'.$row["flightNumber"].'", "'.$days.'","'.$row["carrier"].'"]';
			array_push($res, $route);
		}
	}
	return '{"state":"ok", "origin":"'.$airport.'", "routes":['.implode(",", $res).']}';
*/

	/*orig
		$sql = "SELECT arrivalAirport, destinationcity, departuretime, leg1flightnumber, leg1carriercode, day1, day2, day3, day4, day5, day6, day7 FROM ".$routesTable." GROUP BY leg1flightnumber, arrivalAirport, destinationcity"
				." ORDER BY effectivedate DESC, leg1flightnumber ASC";
		$data = ExecuteQuery($sql, null, $db);
		foreach($data as $row)
		{
			$days = getDays($row);
			$route = '["'.$row["arrivalAirport"].'", "'.$row["destinationcity"].'", "'.$row["departuretime"].'", "'.$row["leg1flightnumber"].'", "'.$days.'","'.$row["leg1carriercode"].'"]';
			array_push($res, $route);
		}
	}
	return '{"state":"ok", "origin":"'.$airport.'", "routes":['.implode(",", $res).']}';
	*/
}

function getFilteredRoutes($db, $airport)
{
	global $database_routes;
	$sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$database_routes."' AND TABLE_NAME=:airport";
	$params = array(":airport"=>$airport);
	$orig_aip = ExecuteQuery($sql, $params, $db);
	
	$res = array();
	if(count($orig_aip)>0)
	{
		$routesTable = $airport;
		
		
		$sql = "SELECT arrivalAirport, departureTime, flightNumber, days, carrier FROM ".$routesTable." where isCodeshare = '0' group by flightNumber, arrivalAirport ORDER BY effectiveDate DESC, flightNumber ASC";
		$data = ExecuteQuery($sql, null, $db);
		foreach($data as $row)
		{
			$days = $row['days'];
			$route = '["'.$row["arrivalAirport"].'", "'.$row["departureTime"].'", "'.$row["flightNumber"].'", "'.$days.'","'.$row["carrier"].'"]';
			array_push($res, $route);
		}
	}
	return '{"state":"ok", "origin":"'.$airport.'", "routes":['.implode(",", $res).']}';
}


function getEquipment($db, $equipment)
{
	global $database_equipment;
	$sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$database_equipment."' AND TABLE_NAME=:equipment";
	$params = array(":iata"=>$equipment);
	$eq_routes = ExecuteQuery($sql, $params, $db);
	
	$res = array();
	if(count($eq_routes)>0)
	{
		$routesTable = $equipment;

		$sql = "SELECT departureAirport, arrivalAirport, departureTime, flightNumber, days, carrier FROM ".$routesTable." where stops = '0' and isCodeshare = '0' group by flightNumber, departureAirport ORDER BY effectiveDate DESC, flightNumber ASC";
		$data = ExecuteQuery($sql, null, $db);
		foreach($data as $row)
		{
			$days = $row['days'];
			$route = '["'.$row["arrivalAirport"].'", "'.$row["departureTime"].'", "'.$row["flightNumber"].'", "'.$days.'","'.$row["carrier"].'"]';
			array_push($res, $route);
		}
	}
	return '{"state":"ok", "origin":"'.$airport.'", "routes":['.implode(",", $res).']}';
}

///////////////////////////////////////////////////// main code ////////////////////////////////////////////////////////
if (defined("DEBUG_MODE")) WriteToLog(">map_functions.php request:\n".print_r($_REQUEST,true));
ob_start("ob_gzhandler");
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Thu, 1 Jan 1970 00:00:00 GMT');
header('Content-Type: application/json');

$ap = $op = "";
if(isset($_REQUEST['op']))
	$op = $_REQUEST['op'];
$filter = "";
if(isset($_REQUEST['filter']))
	$filter = $_REQUEST['filter'];

if($op == "routes"){
	if(isset($_REQUEST['airport']))
	{
		$ap = $_REQUEST['airport'];
		$res = getRoutes($db_routes, $db_airp, $ap, $filter);
	}
	else
	{
		$res = '{"state":"error", "message":"Airport parameter is empty."}';
	}
}

if($op == "equipment"){
	if(isset($_REQUEST['equipment']))
	{
		$eq = $_REQUEST['equipment'];
		$res = getEquipment($db_equipment, $eq);
	}
	else
	{
		$res = '{"state":"error", "message":"Equipment parameter is empty. '.$eq.'"}';
	}
}

else if($op == "allData")
{
	$res = getData($db_airp,$filter);
}

echo($res);
if (defined("DEBUG_MODE")) file_put_contents(LOG_DIR."res.txt",$res);

$db_airp = $db_routes = $db_equipment = null;
exit(0);
?>