/*
 * Application
 * Copyright (c)2014 
 * 
 * @require  google maps v3
 * @require  jQuery
 * @require  infobox.js - infowindow
 * @require  underscore.js - templates
 */


window.onerror = function(msg, url, linenumber){
	var logerror='Error message: ' + msg + '. \nUrl: ' + url + '\nLine Number: ' + linenumber
	//alert(logerror);
	console.log("Error:");
	console.log(logerror);
	//_operationInProgress = false;
	//if(animationHandler)
	//	clearInterval(animationHandler);
		
	//hideWait();
	//jAlert("Error");
}

$(document).ready(function() {
	// init app
	window.isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
	window.app = new App(settings);
});

/************************************************
* class Airport - represents airport on the map
* @params   id		string		- IATA code of airport
* @params   city	string		- city of airport
* @params   lat		double		- latitude of airport location
* @params   lng		double      - longitude of airport location
* @params   map		object      - google.maps.Map object
* @return   instance of class
***********************************************/
function Airport(id, city, lat, lng, rank_tier, map )
{
	var _this = this; // instance of this class
	this.id = id; // IATA code
	this.city = city; 
	this.position = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
	this.selected = false;
	this.map = map;
	this.popup = null;
	this.rank_tier = rank_tier;
	var _size = "normal";
	var _largeSize = 12;
	var _normalSize = 8;
	var _anchorSize = _largeSize - rank_tier;
//console.log("id = "+id+" city = "+city+" rank_tier = "+rank_tier+" _anchorSize = "+_anchorSize);

	// icons styles
	var icons = 
		{
			
			big : 
			{
				url:"images/airp.png",
				//anchor: new google.maps.Point(_anchorSize, _anchorSize),
				anchor: new google.maps.Point(_anchorSize/2, _anchorSize/2),
				scaledSize: new google.maps.Size(_anchorSize, _anchorSize),
				size: new google.maps.Size(_anchorSize, _anchorSize)
			},
			
			normal : 
			{
				url:"images/airp.png",
				//anchor: new google.maps.Point(_anchorSize, _anchorSize),
				anchor: new google.maps.Point(_anchorSize/2, _anchorSize/2),
				scaledSize: new google.maps.Size(_anchorSize, _anchorSize),
				size: new google.maps.Size(_anchorSize, _anchorSize)
			},
			selected : 
			{
				url:"images/aip_sel.png",
				//anchor: new google.maps.Point(_anchorSize, _anchorSize),
				anchor: new google.maps.Point(_anchorSize/2, _anchorSize/2),
				scaledSize: new google.maps.Size(_anchorSize, _anchorSize),
				size: new google.maps.Size(_anchorSize, _anchorSize)
			},
			highlight : 
			{
				url:"images/aip_over.png",
				//anchor: new google.maps.Point(_anchorSize, _anchorSize),
				anchor: new google.maps.Point(_anchorSize/2, _anchorSize/2),
				scaledSize: new google.maps.Size(_anchorSize, _anchorSize),
				size: new google.maps.Size(_anchorSize, _anchorSize)
			}
		};
	
	/************************************************
	* setMap - sets map object of airport graphic
	* @params   map		object      - google.maps.Map object
	***********************************************/
	this.setMap = function(map)
	{
		if(_this.map != map){
			_this.map = map;
			this.graphic().setMap(map);
		}
	}
	
	/************************************************
	* getName - gets name of airport
	* @return	string	- name of airport
	***********************************************/
	this.getName = function()
	{
		return _this.city + " (" + _this.id + ")";
	}
	
	var _graphic;
	/************************************************
	* graphic - gets airport graphic
	* @return	instance of google.maps.Marker
	***********************************************/
	this.graphic = function()
	{
		if(!_graphic){

			var iconObj = {
				url:"images/airp.png",
				//anchor: new google.maps.Point(_anchorSize, _anchorSize),
				anchor: new google.maps.Point(_anchorSize/2, _anchorSize/2),
				scaledSize: new google.maps.Size(_anchorSize, _anchorSize),
				size: new google.maps.Size(_anchorSize, _anchorSize)
			};

			_graphic = new google.maps.Marker({
				map: _this.map,
				position: _this.position,
				icon: iconObj
			});
			
			google.maps.event.addListener(_graphic, 'click', function(){ 
				if(_this.onClick) 	
					_this.onClick(_this);
			});
				
			google.maps.event.addListener(_graphic, 'mouseout', function(event) {
				if(_this.onMouseOut) 	
					_this.onMouseOut(_this);
			});
			google.maps.event.addListener(_graphic, 'mouseover', function(event) {
					if(_this.onMouseOver) 	
					_this.onMouseOver(_this);
			});
		}
		return _graphic;
	};
	
	/************************************************
	* setDeafultSize - sets default size of airport graphic
	***********************************************/
	this.setDeafultSize = function()
	{
		if(_size != "normal")
		{
			_size = "normal";
			
			for(var i in icons)
			{
				icons[i].anchor = new google.maps.Point(_anchorSize/2, _anchorSize/2); // _anchorSize * 0.5
				icons[i].scaledSize =  new google.maps.Size(_anchorSize,_anchorSize);
				icons[i].size = new google.maps.Size(_anchorSize,_anchorSize);
			}
			
			var icon = this.graphic().getIcon();
			icon.anchor = new google.maps.Point(_anchorSize/2,_anchorSize/2);
			icon.scaledSize =  new google.maps.Size(_anchorSize,_anchorSize);
			icon.size = new google.maps.Size(_anchorSize,_anchorSize);
			
			this.graphic().setIcon(icon);
		}
	}
	
	/************************************************
	* setLargeSize - sets large size of airport graphic
	***********************************************/
	this.setLargeSize = function()
	{
		if(_size != "large")
		{
			_size = "large";
			
			for(var i in icons)
			{
				icons[i].anchor = new google.maps.Point(_anchorSize * 1.5/2, _anchorSize * 1.5/2);
				icons[i].scaledSize =  new google.maps.Size(_anchorSize * 1.5,_anchorSize * 1.5);
				icons[i].size = new google.maps.Size(_anchorSize * 1.5,_anchorSize * 1.5);
			}
			
			var icon = this.graphic().getIcon();
			icon.anchor = new google.maps.Point(_anchorSize * 1.5/2,_anchorSize * 1.5/2);
			icon.scaledSize =  new google.maps.Size(_anchorSize * 1.5,_anchorSize * 1.5);
			icon.size = new google.maps.Size(_anchorSize * 1.5,_anchorSize * 1.5);
			
			this.graphic().setIcon(icon);
		}
	}
	
	/************************************************
	* setPopup - sets airport label window
	* @params	popup	instance of InfoBox
	***********************************************/
	this.setPopup = function(popup)
	{
		_this.popup = popup;
		_this.popup.setPosition(_this.graphic().getPosition());
		var content = "<CENTER>" + _this.getName() + "</CENTER>";
		_this.popup.setContent(content);
	}
	
	/************************************************
	* select - selects airport and shows label window
	***********************************************/
	this.select = function()
	{
		_this.selected = true;
		_this.tempSelected = false;
		_this.graphic().setIcon(icons.selected);
		if(_this.popup){
			_this.popup.setMap(_this.map);
			_this.popup.show();
		}
	}
	
	/************************************************
	* tempSelect - shows temporary label window
	***********************************************/
	this.tempSelect = function()
	{
		if(!_this.selected)
		{
			_this.tempSelected = true;
			if(_this.popup){
				_this.popup.setMap(_this.map);
				_this.popup.show();
			}
		}
	}
	this.tempSelected = false;
	
	/************************************************
	* deselect - deselects airport and hides label window
	* @params	popup	instance of InfoBox
	***********************************************/
	this.deselect = function()
	{
		_this.selected = false;
		_this.tempSelected = false;
		_this.graphic().setIcon(icons.normal);

		if(_this.popup && !_this.popup.isHidden_)
			_this.popup.close();
	}
	
	/************************************************
	* highlight - highlights airport
	* @params	popup	instance of InfoBox
	***********************************************/
	this.highlight = function()
	{
		if(_this.popup){
			_this.popup.setMap(_this.map);
			_this.popup.show();
		}
			
		_this.graphic().setIcon(icons.highlight);
	}
	
	/************************************************
	* onClick, onMouseOut, onMouseOver - mouse events
	***********************************************/
	this.onClick = null;	
	this.onMouseOut = null;
	this.onMouseOver = null;
	
	// initialize graphic
	this.graphic();
}

/************************************************
* class Airline - represents airline of Flight object
* @params   id		string		- IATA code of airline
* @params   name	string		- name of airline
* @params   alliance	string	- alliance of the airline
* @return   instance of class
***********************************************/
function Airline(id, name, alliance)
{
	this.id = id;
	this.name = name;
	this.alliance = alliance;
	
	/************************************************
	* compare - compares instance of this class with other airline
	* @params	airline	instance of Airline
	***********************************************/
	this.compare = function(airline)
	{
		if(this.name == airline.name 
			&& this.id == airline.id 
			&& this.alliance == airline.alliance)
			return true
		else 
			return false
	}
}

/************************************************
* class Flight - represents flight of route object on the map
* @params   airline		Airline		- instance of Airline
* @params   startTime	string		- time of departure
* @params   number		string		- flight number
* @params   days		int			- days of flights (in decimal format - days in binary format)
* @return   instance of class
***********************************************/
function Flight(airline, startTime, number, days)
{
	var _this = this;
	this.airline = airline;
	this.startTime = startTime;
	this.number = number;
	this.days = days;
	
	/************************************************
	* getName - gets name of flight
	* @return 	string
	***********************************************/
	this.getName = function(){
		return (this.airline ? this.airline.name : "No Info");
	}
	
	/************************************************
	* getDays - gets days as array[7] of 0 or 1
	*			first day is Monday
	* @return 	array[7] (0,1)
	***********************************************/
	this.getDays = function()
	{
		var res = ["0","0","0","0","0","0","0"];
		if(this.days){
			var daysArr = parseInt(this.days).toString(2).split("");
			res.splice((7 - daysArr.length), daysArr.length, daysArr.join(""));
			res = res.join("").split("");
		}
		return res;
	}
	
	/************************************************
	* toString - represents instance of this class as string
	***********************************************/
	this.toString = function()
	{
		return _this.getName() + "(" + _this.number + ")";
	}
	
	/************************************************
	* compare - compares instance of this class with other flight
	* @params	flight	instance of Flight
	***********************************************/
	this.compare = function(fligth)
	{
		if(((this.airline && fligth.airline && this.airline.compare(fligth.airline)) || (!this.airline && !fligth.airline))
			// && this.startTime == fligth.startTime
			&& this.number == fligth.number 
			//&& this.days == fligth.days
			)
			return true
		else 
			return false
	}
}

/************************************************
* class Filters - represents filters of app
* @params   filterPanelID	string		- id of filters panel (id of DOM element)
* @params   withoutLegID	string		- id of filter by legs (without legs) (id of DOM element)
* @params   with1legID		string		- id of filter by legs (with 1 leg) (id of DOM element)
* @params   daysID			string		- id of filter by days (days panel) (id of DOM element)
* @return   instance of class
***********************************************/
function Filters(filterPanelID, withoutLegID, with1legID, daysID)
{
	this.with1Leg;
	this.withoutLeg;
	this.days = null;
	this.onChanged;
	this.daysVisibleChanged;
	var _this = this;
	var onFilterChangedEvent = function()
	{
		if(_this.onChanged)
			_this.onChanged();
	}
	var onDaysVisibleChangedEvent = function(visible)
	{
		if(_this.daysVisibleChanged)
			_this.daysVisibleChanged(visible);
	}
	
	this._withoutLeg = $("#" + withoutLegID);
	this._with1Leg 	= $("#" + with1legID);
	this._days		= $("#" + daysID);
	this._panel		= $("#" + filterPanelID);
	
	this.updateDays = function()
	{
		if(_this._daysFilter){
			var days = 0;
			_this._daysFilter.find("input:checkbox[name=days]:checked").each(function(){
				days += parseInt(this.value);
			});
			
			if(days == 127)
			{
				_this._daysFilter.find(".selectAll").html("uncheck all").data("state", "selected");
			}
			else
			{
				_this._daysFilter.find(".selectAll").html("check all").data("state", "deselected");
			}
			
			var res = null;
			if(days){
				res = ["0","0","0","0","0","0","0"];
				var daysArr = parseInt(days).toString(2).split("");
				res.splice((7 - daysArr.length), daysArr.length, daysArr.join(""));
				res = res.join("").split("");
			}
			_this.days = res;
		}
	}
	
	this.checkDays = function(days)
	{
		var result = false;
		if(!_this.days && !days)
			return true;
			
		for(var i =0; i < 7; i++)
		{
			if(_this.days[i] == days[i] && _this.days[i] != "0"){
				return true;
			}
		}
		return result;
	}
	
	_this._days.click(function(){
		var filterID = $(this).data("filter-id");
		if(!_this._daysFilter)
		{
			_this._daysFilter = $("#" + filterID);
			_this._daysFilter.find("input:checkbox[name=days]").change(function(){
				_this.updateDays();
				onFilterChangedEvent();
			});
			_this._daysFilter.find(".selectAll").click(function(e){
				e.preventDefault();
				var selected = $(this).data("state");
				if(selected == "selected")
				{
					_this._daysFilter.find("input:checkbox[name=days]").each(function()
					{
						this.checked = false;
					});
					$(this).html("check all").data("state", "deselected");
					_this.days = 0;
				}
				else
				{
					_this._daysFilter.find("input:checkbox[name=days]").each(function()
					{
						this.checked = true;
					});
					$(this).html("uncheck all").data("state", "selected");
					_this.days = 127;
				}
				_this.updateDays();
				onFilterChangedEvent();
			});
		}
		if(_this._daysFilter.data("visible") == "yes"){
			$(this).removeClass("active");
			_this._daysFilter.fadeOut("fast", function(){onDaysVisibleChangedEvent(false);});
			_this._daysFilter.data("visible", "no");
			_this.days = null;
		}
		else
		{
			$(this).addClass("active");
			_this._daysFilter.fadeIn("fast", function(){onDaysVisibleChangedEvent(true);});
			_this._daysFilter.data("visible", "yes");
			_this.updateDays();
		}
	});
	this._daysFilter;
	
	_this._with1Leg.click(function(){
		if(_this.with1Leg)
		{
			_this.with1Leg = false;
			_this._with1Leg.removeClass("active");
		}
		else
		{
			_this.with1Leg = true;
			_this._with1Leg.addClass("active");
		}
		onFilterChangedEvent();
	});
	
	_this._withoutLeg.click(function(){
		if(_this.withoutLeg)
		{
			_this.withoutLeg = false;
			_this._withoutLeg.removeClass("active");
		}
		else
		{
			_this.withoutLeg = true;
			_this._withoutLeg.addClass("active");
		}
		onFilterChangedEvent();
	});
	
	/************************************************
	* reset - resets state of filters to default
	***********************************************/
	this.reset = function()
	{
		setDefault();
		
		_this.days = null;
		_this._days.removeClass("active");
		if(_this._daysFilter)
		{
			_this._daysFilter
				.data("visible", "no")
				.fadeOut()
				.find(".selectAll")
				.data("state", "unselected")
				.html("check all");
			
			_this._daysFilter.find("input:checkbox[name=days]").each(function()
			{
				this.checked = false;
			});
		}
	}
	
	// this.show = function()
	// {
		// _this._panel.fadeIn();
	// }
	// this.hide = function()
	// {
		// _this.reset();
		// _this._panel.fadeOut();
		// _this._daysFilter
	// }
	
	function setDefault()
	{
		_this.with1Leg = false;
		_this.withoutLeg = true;
		_this._withoutLeg.addClass("active");
		_this._with1Leg.removeClass("active");
	}
	
	setDefault();
}

/************************************************
* class AirRoute - represents route between two or three airports on the map
* @params   id		string		- id of route
* @params   origin	string		- instance of origin Airport
* @params   leg1	string		- instance of Airport which is a stop between origin airport and destination airport
* @params   dest	string		- instance of destination Airport
* @params   map		object		- instance of google.maps.Map
* @params   settings object		- settings for route graphic
* @return   instance of class
***********************************************/
function AirRoute(id, origin, leg1, dest, map, settings)
{
	var _this = this;
	this.origin = origin;
	this.leg1 = leg1;
	this.dest = dest;
	this.selected = false;
	this.map = map;
	this.id = id;
	this._settings = settings;

	this.flights = function(){ return _flights; }
	var _flights = new Array(); // array of Flights	
	
	/************************************************
	* getName - gets name of airport
	* @return	string	- name of airport
	***********************************************/
	this.getName = function()
	{
		var name = _this.origin.city + " - " + _this.leg1.city;
		if(_this.dest.id  != _this.leg1.id)
			name+= " - " + _this.dest.city;
		return name;
	}
	
	/************************************************
	* addFlight - adds flight to route
	***********************************************/
	this.addFlight = function(flight)
	{
		var add = true;
		for(var i in _flights)
		{
			if(_flights[i].compare(flight))
			{
				add = false;
				break;
			}
		}
		if(add)
			_flights.push(flight);
	}
	
	/************************************************
	* setMap - sets map object of airport graphic
	* @params   map		object      - google.maps.Map object
	***********************************************/
	this.setMap = function(map)
	{
		if(_this.map != map){
			_this.map = map;
			_this.graphic().setMap(map);
		
			if(!map){
				if(_this._airplaneIcon && !_this._airplaneIcon.isHidden_) 
					_this._airplaneIcon.close();
			}
			else
			{
				if(_this.selected)
					select();
			}
		}
	}
	this.select = function()
	{
		_this.selected = true;
		_this.graphic().setOptions({
				strokeColor: _this._settings.route.selected.color,
				strokeWeight: _this._settings.route.selected.width,
				zIndex: 1000
			})
	}
	this.deselect = function()
	{
		_this.selected = false;
				
		_this.graphic().setOptions({
				strokeColor: _this._settings.route.def.color,
				strokeWeight: _this._settings.route.def.width,
				zIndex: 1
			});
	}
	
	this.highlight = function()
	{
		if(!_this.selected){
			_this.graphic().setOptions({
					strokeColor: _this._settings.route.highlight.color,
					strokeWeight: _this._settings.route.highlight.width,
					zIndex: 999
				});
		}
	}
	
	var _graphic;
	this.graphic = function()
	{
		if(!_graphic){
			// routes options
			var routeOpts = {
				strokeColor: _this._settings.route.def.color,
				strokeOpacity: 0.9,
				strokeWeight: _this._settings.route.def.width,
				geodesic: true,
				map: _this.map
			};
			_graphic = new google.maps.Polyline(routeOpts);
			// build route from origin, leg and sest
			var path = [_this.origin.graphic().getPosition()];
			
			if(_this.leg1.id != _this.dest.id)
				path.push(_this.leg1.graphic().getPosition());
			
			path.push(_this.dest.graphic().getPosition());
			
			_graphic.setPath(path);
			
			google.maps.event.addListener(_graphic, 'click', function(){ 
				if(_this.onClick) 	
					_this.onClick(_this);
			});
				
			google.maps.event.addListener(_graphic, 'mouseout', function(event) {
				if(_this.onMouseOut) 	
					_this.onMouseOut(_this);
			});
			google.maps.event.addListener(_graphic, 'mouseover', function(event) {
					if(_this.onMouseOver) 	
					_this.onMouseOver(_this);
			});
		}
		return _graphic;
	}
	
	// events 
	this.onClick = null;	
	this.onMouseOut = null;
	this.onMouseOver = null;
	// create graphic
	this.graphic();
}


/************************************************
* class App - represents main logic of application
* @params   settings object		- settings for route graphic
* @return   instance of class
***********************************************/
function App(settings)
{
	// constants
	var Messages =
	{
		infos : 
		{
			selectOrigin 	: "Select a departure airport.",
			selectDest 		: "Select destination airport.",
			selectNewOrigin	: "Select a different origin or retry your search."
		}
	};
	
	var _this = this;
	this.airports = new Array(); // array of airports
	this.airlines = new Array(); // array of airlines
	this.airroutes = new Array(); // array of routes
	var _settings = settings;
	var _originAirport;
	var _destAirport;
	var _mapExtent = new google.maps.LatLngBounds();
	var map;
	var center;
	var zoom = 2;

	//var mapStyle = [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]},{"featureType":"landscape","stylers":[{"color":"#f2e5d4"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}];
	var mapStyle = [{"featureType":"all","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"},{"saturation":"-100"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40},{"visibility":"off"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"off"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#4d6059"}]},{"featureType":"landscape","elementType":"geometry.stroke","stylers":[{"color":"#4d6059"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"color":"#4d6059"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"lightness":21}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#4d6059"}]},{"featureType":"poi","elementType":"geometry.stroke","stylers":[{"color":"#4d6059"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#7f8d89"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#7f8d89"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#7f8d89"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#7f8d89"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#7f8d89"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#7f8d89"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#7f8d89"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#7f8d89"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#2b3638"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#2b3638"},{"lightness":17}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#24282b"}]},{"featureType":"water","elementType":"geometry.stroke","stylers":[{"color":"#24282b"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.icon","stylers":[{"visibility":"off"}]}];
	//var mapStyle = [{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#e3e3e3"}]},{"featureType":"landscape.natural","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"color":"#cccccc"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"transit.station.airport","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#FFFFFF"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]}];
	function init()
	{
		if(!window.isMobile ){
			$( document ).tooltip({track:true});
		}
		
		initGUI();
		_this.filters = new Filters("filtersControl", "leg0", "leg1", "btnDaysFilter");
		_this.filters.onChanged = applyFilter;
		_this.filters.daysVisibleChanged = function(visible)
		{
			onResize();
		}
		initMap();
	}
	this.filters;

	function initGUI()
	{
		$(window).resize(function()
		{
			onResize();
		});
		onResize();
		
		var opts = {
			lines: 11, // The number of lines to draw
			length: 17, // The length of each line
			width: 7, // The line thickness
			radius: 14, // The radius of the inner circle
			corners: 1, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			direction: 1, // 1: clockwise, -1: counterclockwise
			color: '#000', // #rgb or #rrggbb or array of colors
			speed: 0.6, // Rounds per second
			trail: 39, // Afterglow percentage
			shadow: false, // Whether to render a shadow
			hwaccel: false, // Whether to use hardware acceleration
			className: 'spinner', // The CSS class to assign to the spinner
			zIndex: 2e9, // The z-index (defaults to 2000000000)
			top: '50%', // Top position relative to parent
			left: '50%' // Left position relative to parent
		};
		var target = document.getElementById('loading');
		//var spinner = new Spinner(opts).spin(target);
		
		mc_show_carriers_alliances(1);
		$("#btnReset").click(function(){
			showAllAirports();
			_this.filters.reset();
		});
		
		$("button.close").click(function(){
			$(this).parent().fadeOut();
			onResize();
		});
		
		$("button.close", ".clear-input").click(function(){
			$(this).parent().parent().find("input").val("");
		});
		
		$("button.close", ".clear-input").click(function(){
			var $input = $(this).parent().parent().find("input").val("");
			if($input[0].id == "destAirportSearch")
			{
				_destAirport = null;
				applyFilter();
			}
			else
			{
				showAllAirports();
			}
		});
		
		$("#originAirportSearch").keypress(function(e){
			if(e.charCode == 13)
			{
				var $autocompleteItems = $(".ui-menu-item", "#ui-id-1");
				if($autocompleteItems.length == 0)
				{
					$(this).val("");
					showAllAirports();
				}
				else if($autocompleteItems.length == 1)
				{
					var val = $autocompleteItems.find("a").html();
					$(this).val(val);
					autocompleteSelected(val, true);
				}
			}
		});
		
		$("#destAirportSearch").keypress(function(e){
			if(e.charCode == 13)
			{
				var $autocompleteItems = $(".ui-menu-item", "#ui-id-2");
				if($autocompleteItems.length == 0)
				{
					$(this).val("");
					_destAirport = null;
					applyFilter();
				}
				else if($autocompleteItems.length == 1)
				{
					var val = $autocompleteItems.find("a").html();
					$(this).val(val);
					autocompleteSelected(val);
				}
			}
		});
		
		$( "#destAirportSearch" ).val("");
		$( "#originAirportSearch" ).val("");

		$("#btn_reset_filters").on("click",function (){
//console.log("reset filters");
			$("#equipmentSearch1").val($("#equipmentSearch1 option:first").val());
			$("#equipmentSearch2").val($("#equipmentSearch2 option:first").val());
			$(".mc_cb_carrier,.mc_cb_alliance,.mc_cb_program,#wideBody,#narrowBody,#turboEngine,#jetEngine").prop("checked",false);
			if (_originAirport) {
					showWait();
					setTimeout(function(){
						getData({op:"routes", airport:_originAirport.id, filter: getFilterData()}, showRoutes);
					}, 1);
					clearDestAirport();
			}
//			getAllData(function (){showAllAirports()});
		});
		// aircraft filters
		$("#equipmentSearch1").on("change",function(){
//console.log("change aircraft type 1")
			if (_originAirport) {
					showWait();
					setTimeout(function(){
						getData({op:"routes", airport:_originAirport.id, filter: getFilterData()}, showRoutes);
					}, 1);
					clearDestAirport();
			}
			else
				getAllData(function (){showAllAirports()});
		});
		$("#equipmentSearch2").on("change",function(){
//console.log("change aircraft type 2")
			if (_originAirport) {
					showWait();
					setTimeout(function(){
						getData({op:"routes", airport:_originAirport.id, filter: getFilterData()}, showRoutes);
					}, 1);
					clearDestAirport();
			}
			else
			getAllData(function (){showAllAirports()});
		});
		// airlines / alliances filters
		$(".mc_cb_carrier").change(function(){
//console.log("carrier changed");console.log(_originAirport);
			if (_originAirport) {
					showWait();
					setTimeout(function(){
						getData({op:"routes", airport:_originAirport.id, filter: getFilterData()}, showRoutes);
					}, 1);
					clearDestAirport();
			}
		});
		$(".mc_cb_alliance").change(function(){
//console.log("alliance changed");console.log(_originAirport);
			if (_originAirport) {
					showWait();
					setTimeout(function(){
						getData({op:"routes", airport:_originAirport.id, filter: getFilterData()}, showRoutes);
					}, 1);
					clearDestAirport();
			}
		});
		$(".mc_cb_program").change(function(){
//console.log("program changed");console.log(_originAirport);
			if (_originAirport) {
					showWait();
					setTimeout(function(){
						getData({op:"routes", airport:_originAirport.id, filter: getFilterData()}, showRoutes);
					}, 1);
					clearDestAirport();
			}
		});
		$("#wideBody,#narrowBody,#turboEngine,#jetEngine").change(function(){
//console.log("aircraft type changed");console.log(_originAirport);
			if (_originAirport) {
					showWait();
					setTimeout(function(){
						getData({op:"routes", airport:_originAirport.id, filter: getFilterData()}, showRoutes);
					}, 1);
					clearDestAirport();
			}
		});

	}
	
	function onResize()
	{
/*
		var $resultsWrapper = $("#resultsWrapper");

		var res_h = $("#content").height() - $resultsWrapper.position().top - $("#footer").height() - 10;
		$resultsWrapper.height(res_h);
		
		$("#routesListWrapper").css("max-height", Math.max(55, (res_h/2 - 35)) + "px");
		if($(".airlines")[0])
			$(".airlines").css("max-height", Math.max(50,(res_h/2 - 42)) + "px");
			
		$("#footer").css("top", ($("#content").height() - 30) + "px");
*/
		$("#resultsWrapper").css("margin-bottom", "0px");
		var res_h =  $("#resultsWrapper").height();
//console.log("res_h = "+res_h);
		$("#routesListWrapper").css("max-height", Math.max(55, (res_h)) + "px");
		$("#mc_carriers_alliances_main").css("max-height", Math.max(55, (res_h)) + "px");

		/*if($resultsWrapper.hasScrollBar())
		{
			$("#leftPanel").width(245);
			$(".title").css("margin-right", "15px");
		}
		else
		{
			$("#leftPanel").width(230);
			$(".title").css("margin-right", "0px");
		}
		*/
		
		var h = $("#content").height() - $("#header").height() - $("#footer").height();		
		var m_w = $("#content").width() - $("#leftPanel").width() - 22;
		$("#mapPanel").height(h);
		$("#mapPanel").width(m_w);
		$("#leftPanel").height(h-20);
	}

	function initMap() {
		// init geocoder
		geocoder = new google.maps.Geocoder();
		center = new google.maps.LatLng(40.7128,-74.0059);
		// init map
		var mapOptions = {
			backgroundColor: "#24282b",
			zoom: 3,
			center: center,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			minZoom:2,
			maxZoom:17,
			styles:mapStyle
		}
		map = new google.maps.Map($("#map-canvas")[0], mapOptions);
		
		// events for show/hide loader
		google.maps.event.addListener(map, 'dragstart', function() {
			//showWait();
		});
		google.maps.event.addListener(map, 'bounds_changed', function() {
			//showWait();
		});
		google.maps.event.addListener(map, 'zoom_changed', function() {
			currentRadius = 0;
		});

		// map loading handle
		google.maps.event.addListener(map, 'idle', function() 
		{
			hideWait();
			setAirportsGraphicsSize();
		});
		
		//_this._commonAirportPopup = createPopup();
		//_this._origAirportPopup = createPopup();
		//_this._destAirportPopup = createPopup();
		//_this._legAirportPopup = createPopup();
		
		getAllData(null);
	}
	var _operationInProgress = false;
	
	function setAirportsGraphicsSize()
	{
		var zoom = map.getZoom();
		var bounds = map.getBounds();
		for(var i in _this.airports)
		{
			var ai = _this.airports[i];
			if(bounds.contains(ai.position))
			{
				if(zoom < 6)
					ai.setDeafultSize();
				else
					ai.setLargeSize();
			}
		}
	}
	
	function getFilterData(){
		var aircraftType1 = ""+$("#equipmentSearch1").val();
//console.log("aircraftType1 = "+aircraftType1);
		var aircraftType2 = ""+$("#equipmentSearch2").val();
		var carriers_alliances = {"mode": 0, "ids": new Array()};
		if ($("#mc_carriers_alliances_control a:eq(0)").hasClass("mc_active")) {
			// carriers
			carriers_alliances["mode"] = 1;
			$(".mc_cb_carrier").each(function(){
				if ($(this).is(":checked")) {
//console.log("checked - "+$(this).attr("value"));
					carriers_alliances["ids"][carriers_alliances["ids"].length] = $(this).attr("value");
				}
			});
		} else {
			// alliances
			carriers_alliances["mode"] = 2;
			$(".mc_cb_alliance").each(function(){
				if ($(this).is(":checked")) {
//console.log("checked - "+$(this).attr("value"));
					carriers_alliances["ids"][carriers_alliances["ids"].length] = $(this).attr("value");
				}
			});
		}
//console.log("aircraftType2 = "+aircraftType2);
		var ar_programs = new Array();
		$(".mc_cb_program").each(function(){
				if ($(this).is(":checked")) {
//console.log("checked - "+$(this).attr("value"));
					ar_programs[ar_programs.length] = $(this).attr("value");
				}
		});

		var filterData = {
			"aircraft_type1":   aircraftType1, 
			"aircraft_type2":   aircraftType2, 
			"carriers":         carriers_alliances,
			"widebody":         ($("#wideBody").is(":checked")) ? 1 : 0,
			"narrowbody":       ($("#narrowBody").is(":checked")) ? 1 : 0,
			"turboengine":      ($("#turboEngine").is(":checked")) ? 1 : 0,
			"jetengine":        ($("#jetEngine").is(":checked")) ? 1 : 0,
			"programs":         ar_programs
		};
		return filterData;
	}
	
	function getAllData(callback)
	{
//console.log("getAllData");
		if (callback)
			initMap();
		_this.airports = new Array();
		_this.airlines = new Array();
		_this.airroutes = new Array();
		var filterData = getFilterData();
//console.log(filterData);
		showWait();
		_operationInProgress = true;
		getData({op:"allData", "filter": filterData}, 
		function(data){
//console.log(data);
			// caching airports
			for(var i in data.airports)
			{
				var aip = data.airports[i];
//console.log("i = "+i);console.log(aip);
				var aip_id = aip["iata"];
//console.log("aip_id = "+aip_id);
				if(!_this.airports[aip_id])
				{
					var airport = new Airport(aip_id, aip["city"], aip["lat"], aip["lon"], aip["rank_tier"], map);
//console.log(airport);
					airport.setPopup(createPopup());
					airport.onClick = airportClick;	
					airport.onMouseOut = airportMouseOut;
					airport.onMouseOver = airportMouseOver;
					
					_this.airports[airport.id] = airport;
				}
			}
			
			$( "#originAirportSearch" ).autocomplete({
				source:autocompleteSource,
				minLength: 2,
				select:function( event, ui ) {
					var item = ui.item.value;
					autocompleteSelected(item, true);
				}
			});
			
			$( "#destAirportSearch" ).autocomplete({
				source: autocompleteSourceDest,
				minLength: 2,
				select:function( event, ui ) {
					var item = ui.item.value;
					autocompleteSelected(item);
				}
			});
			
			// caching airlines
			for(var i in data.airlines)
			{
				var ail = data.airlines[i];
				var ailID = ail["iata"];
				if(ailID != ""){
				    var airline = new Airline(ail["iata"], ail["airline"], ail["alliance"]);
				    if(!_this.airlines[airline.id])
				    {
					    _this.airlines[airline.id] = airline;
				    }
				}
			}
//console.log("show new data");
			if(callback) {
//console.log("show new data 1");
				callback();
			}
			_operationInProgress = false;
			hideWait();
			showInfoAlert(Messages.infos.selectOrigin);
		});
	}
	function autocompleteSourceDest(request, response)
	{
		autocompleteSource(request, response, true);
	}
	
	function autocompleteSelected(val, origin)
	{
		var startIndex = val.indexOf("(") + 1;
		var id = val.substr(startIndex, 3);
		if(!_originAirport || _originAirport.id != id)
		{
			var airp = _this.airports[id];
			airportClick(airp, origin);
		}
		
		$(".ui-autocomplete").hide();
	}
	
	function autocompleteSource(request, response, dest)
	{
		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
		var res = new Array();
		for(var i in _this.airports)
		{
			if(dest && _originAirport && _originAirport.id == i)
				continue;
			if(matcher.test( _this.airports[i].city ) || matcher.test( _this.airports[i].id ))
				res.push(_this.airports[i].getName());
			
			if(res.length > 10) 
				break;
		}
		response(res);
	}

	// animation section
	var _originAirportArimation;
	function showAnimation(airport)
	{
		if(!_originAirportArimation)
		{
			var opts = {
				fillColor: _settings.animation.fill.color,
				fillOpacity: _settings.animation.fill.opacity,
				strokeColor: _settings.animation.outline.color,
				strokeWeight: _settings.animation.outline.width,
				strokeOpacity: _settings.animation.outline.opacity,
				map:map,
				zIndex:99999
			};
			_originAirportArimation = new google.maps.Circle(opts);
		}
		_originAirportArimation.setCenter(airport.graphic().getPosition());
		runAnimation();
	}
	
	var animationHandler;
	var currentRadius;
	var currentRadiusDelta;
	function runAnimation()
	{
		currentRadius = 0;
		currentRadiusDelta = (_settings.animation.max - _settings.animation.min) / _settings.animation.ticksPerCycle;
		
		if(animationHandler)
			clearInterval(animationHandler);
			
		animationHandler = setInterval(function(){
			if(_originAirportArimation)
			{
				var radius = (currentRadius || _settings.animation.min) + currentRadiusDelta;
				
				currentRadius = (radius >= _settings.animation.max) ? _settings.animation.min : radius;
				var fillOpacity = _settings.animation.fill.opacity * (_settings.animation.fill.opacity - (currentRadius - _settings.animation.min)/(_settings.animation.max - _settings.animation.min));
				var outlineOpacity = _settings.animation.fill.opacity * ((_settings.animation.outline.opacity + 0.2) - (currentRadius - _settings.animation.min)/(_settings.animation.max - _settings.animation.min));
				fillOpacity = Math.max(fillOpacity, 0);
				outlineOpacity = Math.max(outlineOpacity, 0);
				_originAirportArimation.setRadius(pixelsToDistance(currentRadius));
				_originAirportArimation.setOptions({fillOpacity:fillOpacity, strokeOpacity:outlineOpacity});
			}
			else
			{
				clearInterval(animationHandler);
			}
		}, _settings.animation.interval);
	}
	
	function pixelsToDistance(pixels)
	{
		return getMapScale() * pixels;
	}
	// Computes scale in meters per pixel for map zoom and latitute.
	function getMapScale()
	{
		var circumference = 40075040,
			zoom, scale;
	
		zoom = map.getZoom();
	
		scale = circumference / Math.pow(2, zoom + 8);
		return scale;
	}
	
	// end animation section
	
	this._airplaneIcon;
	this._airplaneIcon1;
	function showAirplanesOnRoute(route)
	{
	    clearAirplaneIcons();
	    
	    if(route.leg1.id == route.dest.id)
	    {
	        _this._airplaneIcon = showAirplaneOnRoute(route.origin.graphic().position, route.dest.graphic().position, this._airplaneIcon, 1);
	    }
	    else
	    {
	        _this._airplaneIcon = showAirplaneOnRoute(route.origin.graphic().position, route.leg1.graphic().position, this._airplaneIcon, 1);
	        _this._airplaneIcon1 = showAirplaneOnRoute(route.leg1.graphic().position, route.dest.graphic().position, this._airplaneIcon1, 2);
	    }
	}
	function showAirplaneOnRoute(orig, dest, icon, id)
	{
		var orig_lat = orig.lat(), 
		orig_long 	= orig.lng(), 
		des_lat 	= dest.lat(), 
		des_long 	= dest.lng();
		
		function toRad(Value) {
			return Value * Math.PI / 180;
		}
	
		function toDeg(Value) {
			return Value * 180 / Math.PI;
		}
		
		//var R = 6371; // km
		var dLat = toRad(des_lat - orig_lat);
		var dLon = toRad(des_long - orig_long);
		var lat1 = toRad(orig_lat);
		var lat2 = toRad(des_lat);
		var lon1 = toRad(orig_long);
		var lon2 = toRad(des_long);
	
		var Bx = Math.cos(lat2) * Math.cos(dLon);
		var By = Math.cos(lat2) * Math.sin(dLon);
		var lat3a = Math.atan2(Math.sin(lat1) + Math.sin(lat2),
			Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
		var lon3a = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
	
		// center point:
		var lat3 = toDeg(lat3a);
		var lon3 = toDeg(lon3a);
	
		var dLon = toRad(des_long - lon3);
		var lat1 = toRad(lat3);
	
		var y = Math.sin(dLon) * Math.cos(lat2);
		var x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
		var brng = toDeg(Math.atan2(y, x));
		
		if (!icon) {
			var myOptions = {
				content: '<img src="images/airplane-white.png" id="planeimg' + id + '" />',
				boxStyle: {
					width: "26px",
					height: "26px"
				},
				disableAutoPan: true,
				pixelOffset: new google.maps.Size(-12, -12),
				position: new google.maps.LatLng(lat3, lon3),
				closeBoxURL: "",
				isHidden: false,
				pane: "mapPane",
				enableEventPropagation: false,
				zIndex: -4
			};
			
			icon = new InfoBox(myOptions);
		}
		icon.id = id;
		icon.setPosition(new google.maps.LatLng(lat3, lon3));
		icon.open(map);
		rotate($('#planeimg' + id), brng);
	
		function rotate($object, degrees) {
			$object.css({
				'-webkit-transform': 'rotate(' + degrees + 'deg)',
				'-moz-transform': 'rotate(' + degrees + 'deg)',
				'-ms-transform': 'rotate(' + degrees + 'deg)',
				'-o-transform': 'rotate(' + degrees + 'deg)',
				'transform': 'rotate(' + degrees + 'deg)',
				'zoom': 1
	
			});
		}
	
		google.maps.event.addListenerOnce(icon, 'domready', function () {
			rotate($('#planeimg' + this.id), brng);
		});
		
		return icon;
	}
	
	function routeMouseOut(route)
	{
		if(!route.selected ) {
			if(_this.airroutes[route.id]){
				_this.airroutes[route.id].deselect();
				
				if(!_this.airports[route.leg1.id].selected && !_this.airports[route.leg1.id].tempSelected)
					_this.airports[route.leg1.id].deselect();
				
				if(!_this.airports[route.dest.id].selected && !_this.airports[route.dest.id].tempSelected)
					_this.airports[route.dest.id].deselect();
			}
		}
		selectRouteInResultList(route.id, true, true);
	}
	function airportMouseOut(airp)
	{
		if(!airp.selected && !airp.tempSelected){
			airp.deselect();
		}
	}

	function routeMouseOver(route)
	{
		if(!route.selected){
			if(_this.airroutes[route.id]){
				_this.airroutes[route.id].highlight();
				
				if(!_this.airports[route.leg1.id].selected && !_this.airports[route.leg1.id].tempSelected)
					_this.airports[route.leg1.id].highlight();
				
				if(!_this.airports[route.dest.id].selected && !_this.airports[route.dest.id].tempSelected)
					_this.airports[route.dest.id].highlight();
				
				selectRouteInResultList(route.id, true);
			}
		}
		lockMouseEventForList = false;
	}
	
	function airportMouseOver(aip)
	{
		if(!aip.selected){
			_this.airports[aip.id].highlight();
			//showAirportName(_this.airports[aip.id]);
		}
	}
	
	var lockMouseEventForList = false;

	function routeClick(route)
	{
		// select route
		if(_this._selectedRoute && _this._selectedRoute.id != route.id){
			_this._selectedRoute.deselect();
			if(_this._selectedRoute.leg1.id != route.leg1.id)
				_this.airports[_this._selectedRoute.leg1.id].deselect();
			if(_this._selectedRoute.dest.id != route.dest.id)
				_this.airports[_this._selectedRoute.dest.id].deselect();
		}
			
		route.select();
		_this._selectedRoute = _this.airroutes[route.id];
		selectRouteInResultList(route.id);
		
		// show popups for selected airports
		_this.airports[route.leg1.id].tempSelect();
		_this.airports[route.dest.id].tempSelect();
		
		// show airplane icons
		showAirplanesOnRoute(_this._selectedRoute);
		
		showRouteDescription(_this._selectedRoute.id);
	}
	function airportClick(airport, newSearch)
	{
		if(newSearch){
			showWait();
			setVisibleSearchCloseButton("originAirportSearch", true);
			setTimeout(function(){
				getData({op:"routes", airport:airport.id, filter: getFilterData()}, showRoutes);
			}, 1);
		}
		else
		{
			// if select selected airport
			if(airport.selected){
				// if select origin airport once more
				if( _originAirport && airport.id == _originAirport.id)
				{
					showAllAirports();
				}
				else // if select destination airport once more
				{
					showWait();
					setTimeout(function(){
						getData({op:"routes", airport:airport.id, filter: getFilterData()}, showRoutes);
					}, 1);
					clearDestAirport();
				}
			}
			// select destination airport
			else if(_originAirport && airport.id != _originAirport.id)
			{
				_destAirport = airport;
				setVisibleSearchCloseButton("destAirportSearch", true);
				$( "#destAirportSearch" ).val(_destAirport.getName());
				showRoutes(_this._routesData, function(){
					_this.airports[airport.id].select();
				});
				
			}
			else // select origin airport
			{
				showWait();
				setVisibleSearchCloseButton("originAirportSearch", true);
				setTimeout(function(){
					getData({op:"routes", airport:airport.id, filter: getFilterData()}, function(data){
						clearDestAirport();
						showRoutes(data);
					});
				}, 1);
			}
		}
	}
	
	function clearDestAirport()
	{
		$( "#destAirportSearch" ).val("");
		setVisibleSearchCloseButton("destAirportSearch", false);
		_destAirport = null;
	}
	
	function setVisibleSearchCloseButton(id, visible)
	{
		if(visible)
			$( "#" + id ).parent().find(".clear-input").fadeIn();
		else
			$( "#" + id ).parent().find(".clear-input").fadeOut();
	}
	
	this._selectedRoute = null;
	
	function selectRouteInResultList(routeID, onlyHover, removeHover)
	{	
		if(lockMouseEventForList)
			return;
			
		var $routesList = $("#routesListWrapper");
		var $routesListItems = $routesList.find("p");
		if($routesListItems)
		{
			var $selectedItem = $routesList.find('[route-id="' + routeID + '"]');

			if(onlyHover)
			{
				$routesListItems.removeClass("hover");
				if(!removeHover)
					$selectedItem.addClass("hover");
			}
			else
			{
				// deselect all
				$routesListItems.removeClass("selected");
				// find and select route in list
				$selectedItem.addClass("selected");
			}
			if(!removeHover)
			{
				// scroll to selected route in list
				var optionTop = $selectedItem.offset().top;
				var selectTop = $routesList.offset().top;
				
				$routesList.scrollTop($routesList.scrollTop() + (optionTop - selectTop));
			}
		}
	}
	
	function showAllAirports()
	{
//console.log("showAllAirports");
		showWait();
		setTimeout(function(){
			_destAirport = null;
			_originAirport = null;
			
			$( "#originAirportSearch" ).val("");
			setVisibleSearchCloseButton("originAirportSearch");
			
			clearDestAirport();
			$( "#destAirportSearch" )[0].disabled = true;
			
			clearMapData(true);
			//_this.filters.hide();
			
			_this._routesData = null;
//console.log(_this.airports);
			for(var i in _this.airports)
			{
//if (i <= 10) {console.log("i = "+i);console.log(_this.airports[i]);}
				_this.airports[i].setMap(map);
			}
			
			// zoom to routes extent
			//map.setOptions({zoom:zoom, center:center});
			showInfoAlert(Messages.infos.selectOrigin);
			hideWait();
		}, 1);
	}
	
	function showRoutesList()
	{
		$("#routesResult").hide();
	    var list = new Array();
	    // show routes with one stop
		var route = null;
		
		// sort routes
		 // convert to classic array for sort
		var routes = new Array();
		for(var i in _this.airroutes)
		{
			routes.push(_this.airroutes[i]);
		}
		
		routes = routes.sort(function (a, b){
				var aName = a.getName();
				var bName = b.getName();
				return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
			});
			
	    for(var i in routes)
		{
			route = routes[i];
			// if route is visible - show in result list
			if(route.map)
			{
				var title = route.getName();					
				list.push('<p route-id="'+ route.id +'" class="' + ( list.length % 2 == 0 ? "even" : "odd" )+'"> ' + title + '</p>');
			}
		}
		if(list.length > 0){
			showInfoAlert(Messages.infos.selectDest);
			showResAlert(list.length+ " flight path" + (list.length == 1 ? " " : "s ") + "found.");
			$("#routesList").html(list.join(""));
			$("#routesResult").fadeIn();
			addEventsToResList();
			if(list.length == 1)
				routeClick(route);
		}
		else
		{
			showInfoAlert(Messages.infos.selectNewOrigin);
			showResAlert("Flight paths not found");
		}
		
		onResize();
	}
	
	function addEventsToResList()
	{
		var $routesListItems = $("p", "#routesList");
		
		$routesListItems.click(function(){
			$routesListItems.removeClass("selected");
			$(this).addClass("selected");
			var routeID = $(this).attr("route-id");
			lockMouseEventForList = true;
			routeClick(_this.airroutes[routeID]);
		});
		
		$routesListItems.mouseover(function(){
			var routeID = $(this).attr("route-id");
			lockMouseEventForList = true;
			routeMouseOver(_this.airroutes[routeID]);
		});
		
		$routesListItems.mouseout(function(){
			var routeID = $(this).attr("route-id");
			routeMouseOut(_this.airroutes[routeID]);
		});
	}
	
	// shows airport description in left panel
	function showRouteDescription(routeID)
	{
		$("#routeDetails").hide();
		var route = _this.airroutes[routeID];
		$("#routeDetails").html("");
		
		showResAlert(route.flights().length+" flight" + (route.flights().length == 1 ? " " : "s ") + "selected.");
		
		var title = route.getName();
		var startTime = route.startTime;
		var _template = _.template($('#routesegment-template').html());
		var html = _template({
			routeID: route.id,
			routeName: title,
			routeStartTime : startTime
		});
		$("#routeDetails").append(html);
		
		_template = _.template($('#airline-template').html());
		var flightsHtml = new Array();
		var flights = route.flights();
		flights = flights.sort(function (a, b){
			  var aName = a.toString();
			  var bName = b.toString();
			  return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
			});
		for(var i in flights)
		{
			var f = route.flights()[i];
			var days = convertDaysToStylesArray(f.getDays());
			html = _template({
				airlineName	: f.getName(),
				days		: days,
				flightNumber : f.number,
				startTime 	: f.startTime
			});
			flightsHtml.push(html);
		}
		$(".airlines", "#route_" + route.id).html(flightsHtml.join(""));
		$("#routeDetails").fadeIn();
		//_this.filters.show();
		onResize();
	}
	
	function convertDaysToStylesArray(days)
	{
		var res = new Array();
		for(var i in days)
		{
			res.push(days[i] == "0" ? "inactive" : "active");
		}
		return res;
	}
	
	function applyFilter()
	{
		var selRouteID = _this._selectedRoute ? _this._selectedRoute.id : -1; 
		showRoutes(_this._routesData, function(){
			if(_this.airroutes[selRouteID]){
				lockMouseEventForList = false;
				routeClick(_this.airroutes[selRouteID]);
			}
		});
	}
	
	this._routesData;
	
	// routes section
	function showRoutes(data, callback)
	{
		showWait();
		setTimeout(function(){
			if(data)
			{
				_this._routesData = data;
				clearMapData();
				
				// read data from routes (airlines)
				var routesData = data.routes;
				
				// get origin airport from cache
				var orig = _originAirport = _this.airports[data.origin]; 
				$( "#originAirportSearch" ).val(orig.getName());
				$( "#destAirportSearch" )[0].disabled = false;
				if(orig)
				{
					orig.setMap(map);
					showAnimation(orig);
					orig.select();
					//showAirportName(orig, _this._origAirportPopup);
					//_this.airports[orig.id].selected = true;
				}
				
				// read routes info
				for(var i in routesData)
				{
					var routeInfo = routesData[i];
					
					// get destination and leg airports from cache
					var leg = _this.airports[routeInfo[0]]; // leg
					var dest = _this.airports[routeInfo[1]]; // dest
					var startTime = routeInfo[2]; // start time
					var flightNumber = routeInfo[3]; // start time
					var days = parseInt(routeInfo[4]); // days
					var airlineID = routeInfo[5]; // airline id
					
					// create routes for each destination airports from origin airport
					if(orig && dest && leg)
					{
						var flight = new Flight(_this.airlines[airlineID], startTime, flightNumber, days);
						
						// apply filter by destination airport
						if(!_destAirport || _destAirport.id == dest.id)
							addFilteringRoute(orig, dest, leg, flight);
						
						// add route from origin to stop
						if(leg.id != dest.id)
						{
							if(!_destAirport || _destAirport.id == leg.id)
								addFilteringRoute(orig, leg, leg, flight);
						}
					}
				}
				
				// zoom to routes extent
				if(!_mapExtent.isEmpty()){
					_mapExtent.extend(orig.graphic().getPosition());
					map.fitBounds(_mapExtent);
				}
				
				showRoutesList();
			}	
			hideWait();
			if(callback)
				callback();
		}, 1);
	}
	
	function addFilteringRoute(orig, dest, leg, flight)
	{
		var routeID = orig.id + "_" +leg.id + "_" + dest.id;
					
		// only for routes without leg
		if(_this.airroutes[routeID])
		{
			if(!_this.filters.days || _this.filters.checkDays(flight.getDays()))
				_this.airroutes[routeID].addFlight(flight);
		}
		else
		{
			var routeMap = map;
			if(_this.filters.with1Leg && leg.id != dest.id
				|| _this.filters.withoutLeg && leg.id == dest.id
				&& (!_this.filters.days || _this.filters.checkDays(flight.getDays())))
			{
				// show markers for destinations airports	
				dest.setMap(routeMap);
				
				// show markers for stop airports	
				leg.setMap(routeMap);
		
				var route = new AirRoute(routeID, orig, leg, dest, routeMap, _settings);
				
				route.addFlight(flight);
				
				route.onClick = routeClick;	
				route.onMouseOut = routeMouseOut;
				route.onMouseOver = routeMouseOver;
				
				_this.airroutes[routeID] = route;
				
				_mapExtent.extend(dest.graphic().getPosition());
			}
		}
	}
	// end routes section
	
	function createPopup()
	{
		var borderStyle = _settings.airportsInfoPopup.outline.width + "px solid" + _settings.airportsInfoPopup.outline.color;
		var popup = new InfoBox(
			{
				boxStyle: {
					border: borderStyle
					,textAlign: "center"
					,fontSize: _settings.airportsInfoPopup.text.size + "px"
					,fontWeight: "normal"
					,width: "auto"
					,whiteSpace:"nowrap"
					,color: _settings.airportsInfoPopup.text.color
					,borderRadius: "3px"
					,backgroundColor: _settings.airportsInfoPopup.fill.color
					,padding:"2px 4px"
					,opacity:_settings.airportsInfoPopup.opacity
				 }
				,disableAutoPan: true
				,pixelOffset: new google.maps.Size(10, -15)
				,closeBoxURL: ""
				,visible: false
				,pane: "mapPane"
				,enableEventPropagation: true
				,map:map
			});
		google.maps.event.addListenerOnce(popup, 'domready', function () {
			$(".infoBox").parent().css("z-index", 999);
		});
		
		return popup;
	}
	
	// helper functions
	function getData(data, callback)
	{
		$.ajax({
			type: "POST",
			url: "php/map_functions.php",
			dataType: "JSON",
			data:data,
			success: function (data, xhr, options) {
				if(data.state == "ok")
				{
					if(callback)
						callback(data);
				}
				else
				{
					jAlert(data.message, "Error");
				}
				
			},
			error:function(err)
			{
				_operationInProgress = false;
				hideWait();
				console.log("Error:");
				console.log(err);
				jAlert("Error of getting data from server. Please try again later.", "Server error");
			}
		});
	}
	
	function showWait()
	{
		$("#loading").show();
	}
	function hideWait()
	{
		if(!_operationInProgress)
			$("#loading").hide();
	}

	function jAlert(output_msg, title_msg)
	{
		if (!title_msg)
			title_msg = 'Alert';

		if (!output_msg)
			output_msg = '';

		$("<div></div>").html(output_msg).dialog({
			title: title_msg,
			resizable: false,
			modal: true,
			buttons: {
				"Ok": function() 
				{
					$( this ).dialog( "close" );
				}
			}
		});
	}

	function showInfoAlert(msg)
	{
		$("#alertInfo").hide();
		$("#alertInfoText").html(msg);
		$("#alertInfo").fadeIn();
	}

	function showResAlert(msg)
	{	
		$("#alertResult").hide();
		$("#alertResultText").html(msg);
		$("#alertResult").fadeIn();
	}
	
	function hideFilters()
	{
		
	}
	
	function showFilters()
	{
	}
	
	function setVisible(control, visible)
	{
		var $ctrl = $("#" + control);
		if(visible)
			$ctrl.show();
		else
			$ctrl.hide();
	}
	
	function clearMapData(ignoreMarkers)
	{
		for(var i in _this.airports)
		{
			if(_this.airports[i].map)
			{
				if(!ignoreMarkers)
					_this.airports[i].setMap(null);
				_this.airports[i].deselect();
			}
		}
		
		for(var i in _this.airroutes)
		{
			_this.airroutes[i].setMap(null);
		}
		_this.airroutes = new Array()
		
		_originAirport = null;
		
		_mapExtent = new google.maps.LatLngBounds();
		
		if(_originAirportArimation){
			_originAirportArimation.setMap(null);
			_originAirportArimation = null;
		}
		
		clearAirplaneIcons();
		
		$("#routesResult").hide();
		$("#routeDetails").hide();
		setVisible("alertResult", false);
	}
	
	function clearAirplaneIcons()
	{
	    if(_this._airplaneIcon)
			_this._airplaneIcon.close();
		if(_this._airplaneIcon1)
			_this._airplaneIcon1.close();
	} 

	//end  helper functions
	
	init();
}

(function($) {
	$.fn.hasScrollBar = function() {
		return this.get(0).scrollHeight > this.height();
	}
})(jQuery);

function mc_show_carriers_alliances(mode){
	if (mode == 1) {
		// carriers
		$("#mc_carriers_alliances_control a:eq(0)").addClass("mc_active");
		$("#mc_carriers_alliances_control a:eq(1)").removeClass("mc_active");
		$("#mc_carriers_alliances_2").hide();
		$("#mc_carriers_alliances_1").show();
	} else {
		// alliances
		$("#mc_carriers_alliances_control a:eq(0)").removeClass("mc_active");
		$("#mc_carriers_alliances_control a:eq(1)").addClass("mc_active");
		$("#mc_carriers_alliances_1").hide();
		$("#mc_carriers_alliances_2").show();
	}
}