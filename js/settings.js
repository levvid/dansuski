var settings =
{
	route:
		{
			def 		: { color : "#8181F7", width : 1 },
			highlight 	: { color : "#0000FF", width : 2 },
			selected 	: { color : "#0000FF", width : 2 },
		},
	animation : 
		{
			fill : 		{ color : "#4888EF", opacity : 0.9 },
			outline : 	{ color : "#0088EF", opacity : 0.9, width : 1 },
			min : 1,  // px
			max : 30, // px
			interval : 65, // ms
			ticksPerCycle : 30
		},
	airportsInfoPopup :
		{
			fill : 		{ color : "#333" },
			outline : 	{ color : "#bbb", width : 1 }, 
			text :		{ color : "#f8f8f8", size: 12},
			opacity : 0.9
		}
/*	airportsInfoPopup :
		{
			fill : 		{ color : "#f8f8f8" },
			outline : 	{ color : "#bbb", width : 1 },
			text :		{ color : "#333", size: 12},
			opacity : 0.9
		}
/*	airportsInfoPopup :
		{
			fill : 		{ color : "#4888EF" },
			outline : 	{ color : "#0088EF", width : 0 },
			text :		{ color : "#FFF", size: 12},
			opacity : 0.9
		}*/
}