<?php
$page = 'map';
require_once($_SERVER['DOCUMENT_ROOT'].'/php/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db_config.php');

$country = file_get_contents("https://www.seatlink.com/php/tools/geo/geoip/?ip=".$_SERVER['REMOTE_ADDR']);
$center_lat = preg_match('/<lat>(.*?)<\/lat>/',$country,$lat);
$center_lat = $lat[1];
$center_lon = preg_match('/<lon>(.*?)<\/lon>/',$country,$lon);
$center_lon = $lon[1];

$db2 = new PDO('mysql:host=localhost;dbname='.$database_airports.';charset=utf8', $username, $password);
$q1 = $db2->query("SELECT iata,name FROM equipment WHERE destinations IS NOT NULL ORDER BY name");
$data_equipment = $q1->fetchAll(PDO::FETCH_ASSOC);

$q1 = $db2->query("SELECT id,name FROM airlines_alliances ORDER BY name");
$data_aliances = $q1->fetchAll(PDO::FETCH_ASSOC);

$q1 = $db2->query("SELECT iata,airline FROM airlines where active = 1 or active = 2 ORDER BY airline");
$data_airlines = $q1->fetchAll(PDO::FETCH_ASSOC);

$q1 = $db2->query("SELECT id,program_name FROM credit_cards_programs ORDER BY program_name");
$data_programs = $q1->fetchAll(PDO::FETCH_ASSOC);

$uri = preg_replace("/\?(.*)/","",$_SERVER['REQUEST_URI']);
$urls = explode("/",$uri);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="/images/favicon.png">

	<title>What Flies Where</title>

	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.css" rel="stylesheet">

	<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="/css/bootstrap-theme.min.css"> --> <!--removed in v4-->
	<link rel="stylesheet" href="/css/additional.css">
	<link rel="stylesheet" href="/css/custom_bootstrap.css" >
	<link rel="stylesheet" href="/css/styles.css">
	<link rel="stylesheet" href="/css/cupertino/jquery-ui-1.10.4.custom.min.css">

	<!-- glyphicons -->
	<link rel="stylesheet" href="/css/glyphicons/css/glyphicons.css">
	<link rel="stylesheet" href="/css/glyphicons/css/halflings.css">
	<link rel="stylesheet" href="/css/glyphicons/css/social.css">


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.js"></script>

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&language=en&key=AIzaSyC8cwTrrAKrm3dux2tZDRIxB3k-IDgfm9k"></script>
	<script type="text/javascript" src="/js/underscore.js"></script>
	<script type="text/javascript" src="/js/infobox.js"></script>
	<script type="text/javascript" src="/js/settings.js"></script>
	<script type="text/javascript" src="/js/spin.js"></script>
	<script type="text/javascript" src="/js/app.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="/js/ie10-viewport-bug-workaround.js"></script>

</head>
<body>
	<? require_once($_SERVER['DOCUMENT_ROOT'].'/php/gtm.php'); ?>
	<div id="content">

		<?
		$uri = preg_replace("/\?(.*)/","",$_SERVER['REQUEST_URI']);
		$urls = explode("/",$uri);
		?>

		<!--
		<div id="header" class="navbar navbar-default">
		<div class="navbar-header title">
		<span class="navbar-brand">Air Lines</span>
	</div>
	<form class="navbar-form navbar-left navbar-search-panel" role="search">
	<div class="form-group">
	<div class="input-group input-group-sm">
	<span class="navbar-search-text">Search:</span>
</div>
<div class="input-group input-group-sm">
<span class="input-group-addon originIcon"></span>
<span  class="clear-input"><button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></span>
<input id="originAirportSearch" type="text" class="form-control" placeholder="Origin">
</div>
<div class="input-group input-group-sm">
<span class="input-group-addon destIcon"></span>
<span  class="clear-input"><button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></span>
<input id="destAirportSearch" type="text" class="form-control" disabled placeholder="Destination">
</div>
</div>
</form>
</div>
-->


<!-- Fixed navbar -->
<div class="navbar fixed-top navbar-expand-sm" role="navigation" id="navbar-home">
	<div class="container">
		<button type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/"><img src="/images/logo-bla.png" alt="WhatFliesWhere" style="margin:-12px 0 3px 10px; padding:0px;" width="181px" height="40px"><!--<sub style="font-size:0.5em; color:#ccc"><em>beta</em></sub>--></a>
	</div>
</div>



<!-- Begin page content -->
<div class="container">
	<div id="leftPanel">
		<div class="card card-success">
			<div class="card-header" role="tab" id="headingOne">
				<h4 class="card-title">
					<a data-toggle="collapse" href="#collapseOne" aria-expanded="tue" aria-controls="collapseExample">
						Search
					</a>
				</h4>
			</div>
			<div id="collapseOne" class="card-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				<div class="card-block">
					<form class="form" role="form">
						<div class="form-group">
							<div class="input-group input-group-sm col-md-12">
								<span class="input-group-addon originIcon"></span>
								<span class="clear-input"><button type="button" class="close">
									<span aria-hidden="true">×</span>
									<span class="sr-only">Close</span>
								</button>
							</span>	<span class="ui-helper-hidden-accessible" aria-live="polite" role="status"></span>
							<input autocomplete="off" id="originAirportSearch" class="form-control ui-autocomplete-input" placeholder="Origin" type="text">
						</div>
						<br>
						<div class="input-group input-group-sm col-md-12">
							<span class="input-group-addon destIcon"></span>
							<span class="clear-input"><button type="button" class="close">
								<span aria-hidden="true">×</span>
								<span
								class="sr-only">Close</span>
							</button>
						</span>
						<span class="ui-helper-hidden-accessible" aria-live="polite" role="status"></span>
						<input autocomplete="off" id="destAirportSearch" class="form-control ui-autocomplete-input" disabled placeholder="Destination" type="text">
					</div>
				</div>
			</form>
			<!-- results used to be here! -->
		</div>
	</div>
</div>


<div class="card card-success">
	<div class="card-header" role="tab" id="headingOne">
		<h4 class="card-title">
			<a data-toggle="collapse" href="#collapseOne1" aria-expanded="tue" aria-controls="collapseExample">
				Results
			</a>
		</h4>
	</div>
	<div id="collapseOne1" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">
	<div class="card-block">
		<div id="airport-filters">
			<!--<h4>Results </h4>-->
			<div id="resultsWrapper">
				<div id="routesResult" class="resPanel">
					<div id="routesResultListHeader">Flights</div>
					<div id="routesListWrapper">
						<div id="routesList"></div>
					</div>
				</div>
				<div id="routeDetails" class="resPanel"></div>
			</div>
		</div>
	</div>
</div>
</div>


<div class="card card-success">
	<div class="card-header" role="tab" id="headingTwo">
		<h4 class="card-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
				Aircraft filters
			</a>
		</h4>
	</div>
	<div id="collapseTwo" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingTwo">
	<div class="card-block">
		<div class="form-group">
			<select id="equipmentSearch1" class="form-control">
				<option selected="selected" value="choose">select a plane...</option>
				<?
				foreach($data_equipment as $row) { if ((preg_match('/Freighter/',$row['name'])) || (strlen($row['iata']) != 3))  continue; ?>
					<option value="<?=$row['iata']; ?>"><?=$row['name']; ?></option>
				<? } ?>
			</select>
		</div>
		<div class="form-group">
			<select id="equipmentSearch2" class="form-control">
				<option selected="selected" value="choose">select another plane...</option>
				<option selected="selected" value="choose">select another plane...</option>
		<?
			foreach($data_equipment as $row) { if ((preg_match('/Freighter/',$row['name'])) || (strlen($row['iata']) != 3))  continue; ?>
			<option value="<?=$row['iata']; ?>"><?=$row['name']; ?></option>
		<? } ?>
			</select>
		</div>
		<div class="form-group aircrafttype">
			<input type="checkbox" id="wideBody">Wide-body aircraft<br>
			<input type="checkbox" id="narrowBody">Narrow-body aircraft<br>
			<input type="checkbox" id="turboEngine">Turboprop-engined aircraft<br>
			<input type="checkbox" id="jetEngine">Jet-engined aircraft<br>
		</div>
	</div>
</div>
</div>
<div class="card card-success">
	<div class="card-header" role="tab" id="headingThree">
		<h4 class="card-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
				Airline/Alliance filters
			</a>
		</h4>
	</div>
	<div id="collapseThree" class="card-collapse collapse in" role="tabpanel" aria-labelledby="headingThree"></div>
	<div class="card-block">
		<div id="mc_carriers_alliances_control">
			<a href="javascript:void(0)" onclick="mc_show_carriers_alliances(1)" class="mc_active">Carriers</a>
			&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="javascript:void(0)" onclick="mc_show_carriers_alliances(2)">Alliances</a>
		</div>
		<div id="mc_carriers_alliances_main" style="min-height:300px;">
			<div id="mc_carriers_alliances_1">
				<?php
				foreach ($data_airlines as $row) {
					?>
					<input type="checkbox" id="mc_car_<?=$row["iata"]?>" value="<?=$row["iata"]?>" class="mc_cb_carrier" />
					<label><?=$row["airline"]?></label>
					<br/>
					<?php
				}
				?>
			</div>
			<div id="mc_carriers_alliances_2" style="display: none">
				<?php
				foreach ($data_aliances as $row) {
					?>
					<input type="checkbox" id="mc_car_<?=$row["id"]?>" value="<?=$row["id"]?>" class="mc_cb_alliance" />
					<label><?=$row["name"]?></label>
					<br/>
					<?php
				}
				?>
			</div>
		</div>
		<?php /*
		[show airlines and alliances for a selected route here, with checkboxes that filter them like on kayak.com]
		*/ ?>
	</div>
</div>
</div>


<div class="card card-success">
	<div class="card-header" role="tab" id="headingThree">
		<h4 class="card-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
				Rewards Program filters
			</a>
		</h4>
	</div>
	<div id="collapseThree" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingThree">
		<div class="card-block">
			<div id="mc_programs">
				<?php
				foreach ($data_programs as $row) {
					?>
					<input type="checkbox" id="mc_prog_<?=$row["id"]?>" value="<?=$row["id"]?>" class="mc_cb_program" />
					<label><?=$row["program_name"]?></label>
					<br/>
					<?php
				}
				?>
			</div>
			<?php /*				        [show rewards programs here like the airline/alliance filters] */ ?>

		</div>
	</div>
</div>


<div class="card card-success">
	<button type="button" id="btn_reset_filters">RESET FILTERS</button>
</div>



<div id="resultsWrapper">
	<div id="routesResult" class="resPanel">
		<div id="routesResultListHeader">Flight paths</div>
		<div id="routesListWrapper">
			<div id="routesList">
			</div>
		</div>
	</div>
	<div id="routeDetails" class="resPanel">
	</div>
</div>
</div>
<div class = "container">
	<div id="mapPanel">
		<div id="loading"><img src="../images/loader.gif" /></div>
		<div id="map-canvas" class="map-container"></div>
	</div>
	</div>
</div>
<div id="mapPanel">
	<div id="loading"><img src="../images/loader.gif" /></div>
	<div id="map-canvas" class="map-container"></div>
</div>
</div>
<!-- Site footer -->
<div class="footer">
	<div class="row">
		<div class="float-right footer-social">
			<a href="http://www.facebook.com/GoPointsGo"><span class="social facebook"></span></a><!--&nbsp;<a href="http://plus.google.com/+GoPointsGo"><span class="social google_plus"></span></a>-->&nbsp;<a href="http://www.twitter.com/GoPointsGo"><span class="social twitter"></span></a>&nbsp;<a href="http://www.instagram.com/GoPointsGo"><span class="social instagram"></span></a>&nbsp;<a href="http://www.pinterest.com/GoPointsGo"><span class="social pinterest"></span></a>&nbsp;
		</div>
		<div class="float-left">
			&copy; 2014-<?=date(Y); ?> WhatFliesWhere.com
		</div>
	</div>
</div>
<!--templates-->
<script id="routesegment-template" type="text/template">
	<div id="route_<%=routeID%>" class="routesegment">
		<div class="routename"><%=routeName%></div>
		<div class="airlines">
		</div>
	</div>
</script>
<script id="airline-template" type="text/template">
	<div class="flightairline">
		<div class="airlinename"><%=airlineName%> (<%=flightNumber%>)</div>
		<span><%=startTime%></span>
		<div class="datepick">
			<a class="dayview <%=days[0]%>">mon</a>
			<a class="dayview <%=days[1]%>">tue</a>
			<a class="dayview <%=days[2]%>">wed</a>
			<a class="dayview <%=days[3]%>">thu</a>
			<a class="dayview <%=days[4]%>">fri</a>
			<a class="dayview <%=days[5]%>">sat</a>
			<a class="dayview <%=days[6]%>">sun</a>
		</div>
	</div>
</script>
</body>
</html>
<?
$db2 = $db_airp = $db_routes = $db_equipment = null;
exit(0);
?>
