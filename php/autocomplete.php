<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db_config.php');
mysql_select_db('gopoints_dataset');

$term = trim(strip_tags($_GET['term']));

## airport
if ($_GET['airline']) {
	
	$airlinesearch = $term;
	$airlines = array();
	
	$query = "select * from `airlines` where (`iata` like '$airlinesearch%' || `airline` like '$airlinesearch%') order by `airline`";
	$submit = mysql_query($query) or die (mysql_error());
	
	#$i=0;
	while ($r = mysql_fetch_assoc($submit)) {
		$thisairline = $r['airline'];
		$thisiata = $r['iata'];
		#if ($thiscity == $lastcity && $thisiata == $lastiata && $thisairport == $lastairport) continue;
		#$lastcity = $thiscity;
		#$lastiata = $thisiata;
		#$lastairport = $thisairport;
		$airlines[] = array('airline'=>$thisairline,'iata'=>$thisiata,'fullairline'=> $thisairline." (".$thisiata.")");
	} 
	$matches = array();
	foreach($airlines as $airline){
		// Add the necessary "value" and "label" fields and append to result set
		#$cock = $city['city']." - ".$city['state'];
		$airline['value'] = $airline['fullairline'];
		$ariline['label'] = "{$airline['fullairline']}";
		$matches[] = $airline;
	}		
	
}

## city
if ($_GET['city']) {

	$citysearch = $term;
	$cities = array();
	
	$query = "select * from `stations` where (`iata` like '$citysearch%' || `city` like '$citysearch%' || `name` like '$citysearch%') order by `city`";
	$submit = mysql_query($query) or die (mysql_error());
	
	#$i=0;
	while ($r = mysql_fetch_assoc($submit)) {
		$thiscity = $r['city'];
		$thisiata = $r['iata'];
		$thisairport = $r['name'];
		#if ($thiscity == $lastcity && $thisiata == $lastiata && $thisairport == $lastairport) continue;
		#$lastcity = $thiscity;
		#$lastiata = $thisiata;
		#$lastairport = $thisairport;
		$cities[] = array('city'=>$thiscity,'iata'=>$thisiata,'location'=> $thiscity." - ".$thisairport." (".$thisiata.")");
	} 
	$matches = array();
	foreach($cities as $city){
		// Add the necessary "value" and "label" fields and append to result set
		#$cock = $city['city']." - ".$city['state'];
		$city['value'] = $city['location'];
		$city['label'] = "{$city['location']}";
		$matches[] = $city;
	}	
	
}

## destinations
if ($_GET['destination']) {

	$citysearch = $term;
	$destinations = array();
	$countrycodes = array('Indonesia'=>'id','Papua New Guinea'=>'pg','Mexico'=>'mx','Estonia'=>'ee','Algeria'=>'dz','Morocco'=>'ma','Mauritania'=>'mr','Senegal'=>'sn','Gambia'=>'gm','Guinea-Bissau'=>'gw','Guinea'=>'gn','Sierra Leone'=>'sl','Liberia'=>'lr','Cote d\'Ivoire'=>'ci','Mali'=>'ml','Burkina Faso'=>'bf','Niger'=>'ne','Ghana'=>'gh','Togo'=>'tg','Benin'=>'bj','Nigeria'=>'ng','Tunisia'=>'tn','Libya'=>'ly','Egypt'=>'eg','Chad'=>'td','Sudan'=>'sd','Cameroon'=>'cm','Eritrea'=>'er','Djibouti'=>'dj','Ethiopia'=>'et','Somalia'=>'so','Yemen'=>'ye','Central African Republic'=>'cf','Sao Tome and Principe'=>'st','Equatorial Guinea'=>'gq','Gabon'=>'ga','Congo'=>'cg','Angola'=>'ao','Congo'=>'cd','Rwanda'=>'rw','Burundi'=>'bi','Uganda'=>'ug','Kenya'=>'ke','Tanzania'=>'tz','Zambia'=>'zm','Malawi'=>'mw','Mozambique'=>'mz','Zimbabwe'=>'zw','Namibia'=>'na','Botswana'=>'bw','Swaziland'=>'sz','Lesotho'=>'ls','South Africa'=>'za','Greenland'=>'gl','Australia'=>'au','New Zealand'=>'nz','New Caledonia'=>'nc','Malaysia'=>'my','Brunei Darussalam'=>'bn','Timor-Leste'=>'tl','Solomon Islands'=>'sb','Vanuatu'=>'vu','Fiji'=>'fj','Philippines'=>'ph','China'=>'cn','Taiwan'=>'tw','Japan'=>'jp','Russia'=>'ru','United States'=>'us','Mauritius'=>'mu','Reunion'=>'re','Madagascar'=>'mg','Comoros'=>'km','Seychelles'=>'sc','Maldives'=>'mv','Portugal'=>'pt','Spain'=>'es','Cape Verde'=>'cv','French Polynesia'=>'pf','Saint Kitts and Nevis'=>'kn','Antigua and Barbuda'=>'ag','Dominica'=>'dm','Saint Lucia'=>'lc','Barbados'=>'bb','Grenada'=>'gd','Trinidad and Tobago'=>'tt','Dominican Republic'=>'do','Haiti'=>'ht','Falkland Islands'=>'fk','Iceland'=>'is','Norway'=>'no','Sri Lanka'=>'lk','Cuba'=>'cu','Bahamas'=>'bs','Jamaica'=>'jm','Ecuador'=>'ec','Canada'=>'ca','Guatemala'=>'gt','Honduras'=>'hn','El Salvador'=>'sv','Nicaragua'=>'ni','Costa Rica'=>'cr','Panama'=>'pa','Colombia'=>'co','Venezuela'=>'ve','Guyana'=>'gy','Suriname'=>'sr','French Guiana'=>'gf','Peru'=>'pe','Bolivia'=>'bo','Paraguay'=>'py','Uruguay'=>'uy','Argentina'=>'ar','Chile'=>'cl','Brazil'=>'br','Belize'=>'bz','Mongolia'=>'mn','North Korea'=>'kp','South Korea'=>'kr','Kazakhstan'=>'kz','Turkmenistan'=>'tm','Uzbekistan'=>'uz','Tajikistan'=>'tj','Kyrgyzstan'=>'kg','Afghanistan'=>'af','Pakistan'=>'pk','India'=>'in','Nepal'=>'np','Bhutan'=>'bt','Bangladesh'=>'bd','Myanmar'=>'mm','Thailand'=>'th','Cambodia'=>'kh','Laos'=>'la','Vietnam'=>'vn','Georgia'=>'ge','Armenia'=>'am','Azerbaijan'=>'az','Iran'=>'ir','Turkey'=>'tr','Oman'=>'om','United Arab Emirates'=>'ae','Qatar'=>'qa','Kuwait'=>'kw','Saudi Arabia'=>'sa','Syria'=>'sy','Iraq'=>'iq','Jordan'=>'jo','Lebanon'=>'lb','Israel'=>'il','Cyprus'=>'cy','United Kingdom'=>'gb','Ireland'=>'ie','Sweden'=>'se','Finland'=>'fi','Latvia'=>'lv','Lithuania'=>'lt','Belarus'=>'by','Poland'=>'pl','Italy'=>'it','France'=>'fr','Netherlands'=>'nl','Belgium'=>'be','Germany'=>'de','Denmark'=>'dk','Switzerland'=>'ch','Czech Republic'=>'cz','Slovakia'=>'sk','Austria'=>'at','Hungary'=>'hu','Slovenia'=>'si','Croatia'=>'hr','Bosnia and Herzegovina'=>'ba','Malta'=>'mt','Ukraine'=>'ua','Moldova'=>'md','Romania'=>'ro','Serbia'=>'rs','Bulgaria'=>'bg','Albania'=>'al','Macedonia'=>'mk','Greece'=>'gr');
	
	$query = "select * from destinations where destination like '$citysearch%' order by destination";
	$submit = mysql_query($query) or die (mysql_error());
	
	#$i=0;
	while ($r = mysql_fetch_assoc($submit)) {
		$thisdestination = $r[destination];
		$thiscountry = $r[country];
		$thisregion = $r[region];
		$thislat = $r[lat];
		$thislon = $r[lon];
		$thiscountrycode = $countrycodes[$thiscountry];
		if ($thiscountrycode == null) $thiscountrycode = 'us';
		$destinations[] = array('destination'=>$thisdestination,'country'=>$thiscountry,'region'=>$thisregion,'lat'=>$thislat,'lon'=>$thislon,'location'=>$thisdestination.", ".$thiscountry,'cc'=>$thiscountrycode);
	} 
	$matches = array();
	foreach($destinations as $destination){
		$destination['value'] = $destination['location'];
		$destination['label'] = "{$destination['location']}";
		$matches[] = $destination;
	}	
	
}


## truncate, encode and return the results
$matches = array_slice($matches, 0, 15);
print json_encode($matches);
mysql_close();
?>